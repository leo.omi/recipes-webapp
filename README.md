# recipe-webapp
Frontend for a recipe app. Uses [recipe-api](https://gitlab.com/leo.omi/recipes-api) as backend.

## Prerequisites
```
Node.js
npm - Node Package Manager
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

If ran this way, uses localhost:8080 as the address for the API. 

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
