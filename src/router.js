import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/recipe',
      name: 'recipeForm',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/RecipeForm.vue')
    },
    {
      path: '/recipes',
      name: 'recipeList',
      component: () => import(/* webpackChunkName: "about" */ './views/RecipeList.vue')
    },
    {
      path: '/recipes/:id',
      name: 'recipeDetails',
      component: () => import(/* webpackChunkName: "about" */ './views/RecipeDetails.vue')
    }
  ]
})
